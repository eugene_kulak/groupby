﻿// console.cpp : Defines the entry point for the console application.
//
// ConsoleApplication1.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"

#include <string>
#include <iostream>
#include <fstream>

#include <map>
#include <unordered_map>
#include <thread>

struct Rec
{
	int date;
	int action;
	int banner_id;
	int browser;
	int device;
};

struct Filter
{
	Filter()
		: value(0)
	{

	}
	std::unordered_map<int, Filter> group;
	int value;
};

Filter& dateFilter(Filter& flt, Rec& rec)
{
	return flt.group[rec.date];
}

Filter& browserFilter(Filter& flt, Rec& rec)
{
	return flt.group[rec.browser];
}

Filter& deviceFilter(Filter& flt, Rec& rec)
{
	return flt.group[rec.device];
}

void print(Filter& flt)
{
	if (flt.group.empty())
	{
		std::cout << flt.value << std::endl;
		return;
	}
	for (auto& it : flt.group)
	{
		std::cout << it.first << ":";
		print(it.second);
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	std::ifstream rawFile;
	std::string line;
	rawFile.open("../../rawdata.csv");
	char buff[646+1] = {0};

	Rec rec = {0};
	Filter flt;
	long i = 0;

	std::unordered_map<std::string, int> bannerMap;
	std::unordered_map<std::string, int> browserMap;
	std::unordered_map<std::string, int> deviceMap;

	std::vector<const std::string*> bannerIdMap;
	std::vector<const std::string*> browserIdMap;
	std::vector<const std::string*> deviceIdMap;

	if (rawFile.is_open())
	{
		while (rawFile)
		{
			rawFile.getline(buff, 646+1);
			if (strlen(buff) == 0)
			{
				continue;
			}
			const char* token = nullptr;
			// id
			token = strtok(buff, ",");
			// skip

			// date
			token = strtok(nullptr, ",");
			{
				rec.date = i;	// TODO
				/*auto it = dateMap.find(token);
				if (it == dateMap.end())
				{
					int i = dateMap.size();
					dateMap[token] = i+1;
					rec.date = i+1;
				}
				else
				{
					rec.date = it->second;
				}*/
			}
			//strcpy(rec.date, token);
			
			// action
			token = strtok(nullptr, ",");
			{
				if (token[0] == 's')
				{
					rec.action = 1;
				}
				else if (token[0] == 'r')
				{
					rec.action = 2;
				}
				else if (token[0] == 'c')
				{
					rec.action = 3;
				}
			}

			// banner_id
			token = strtok(nullptr, ",");
			{
				auto it = bannerMap.find(token);
				if (it == bannerMap.end())
				{
					int i = bannerMap.size();
					i++;
					bannerMap[token] = i;
					bannerIdMap.push_back( &(bannerMap.find(token)->first) );	// not sure about no copy
					rec.banner_id = i;
				}
				else
				{
					rec.banner_id = it->second;
				}
			}

			// browser
			token = strtok(nullptr, ",");
			{
				auto it = browserMap.find(token);
				if (it == browserMap.end())
				{
					int i = browserMap.size();
					i++;
					browserMap[token] = i;
					browserIdMap.push_back( &(browserMap.find(token)->first) );
					rec.browser = i;
				}
				else
				{
					rec.browser = it->second;
				}
			}

			// device
			token = strtok(nullptr, ",");
			{
				auto it = deviceMap.find(token);
				if (it == deviceMap.end())
				{
					int i = deviceMap.size();
					i++;
					deviceMap[token] = i;
					deviceIdMap.push_back( &(deviceMap.find(token)->first) );
					rec.device = i;
				}
				else
				{
					rec.device = it->second;
				}
			}

			deviceFilter(browserFilter(dateFilter(flt, rec), rec), rec).value++;

			std::cout << i++ << std::endl;
			if (i > 100000) break;
		}
		print(flt);
	}

	return 0;
}

